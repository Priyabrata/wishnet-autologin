﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutoLogin
{
    public partial class frmMain : Form
    {
        bool running;
        public frmMain()
        {
            InitializeComponent();
            running = false;
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            timer1.Interval = 5000;
            try
            {
                var fs = File.Open("info.dat", FileMode.Open, FileAccess.Read);
                var sr = new StreamReader(fs);
                txtUrl.Text = sr.ReadLine();
                txtUser.Text = sr.ReadLine();
                txtPwd.Text = sr.ReadLine();
                sr.Close();
                fs.Close();
            }
            catch(Exception ex)
            {
                MessageBox.Show("Please save your login information!");
                return;
            }
        }

        public void forceClick(IWebElement element)
        {
            //https://code.google.com/p/selenium/issues/detail?id=2766 (fix)
            while (true)
            {
                try
                {
                    element.Click();
                    break;
                }
                catch (Exception e)
                {

                }
            }
        }

        public static bool CheckForInternetConnection()
        {
            String []host = { "8.8.8.8",  "8.8.4.4", "208.67.222.222", "208.67.220.220" };
            byte[] buffer = new byte[32];
            int timeout = 1000;
            PingOptions pingOptions = new PingOptions();
            for(int i = 0; i < host.Length; i++)
            {
                try
                {
                    Ping myPing = new Ping();
                    PingReply reply = myPing.Send(host[i], timeout, buffer, pingOptions);
                    myPing.Dispose();
                    if (reply.Status == IPStatus.Success)
                    {
                        return true;
                    }
                }
                catch (Exception ex)
                {
                }
            }

            return false;
        }

        private bool check2()
        {
            Process[] chromeDriverProcesses;
            var driverService = ChromeDriverService.CreateDefaultService();
            driverService.HideCommandPromptWindow = true;
            IWebDriver driver = new ChromeDriver(driverService, new ChromeOptions());
            driver.Navigate().GoToUrl("http://1.1.1.1");
            driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(2));
            string url = driver.Url.ToString();
            driver.Close();
            chromeDriverProcesses = Process.GetProcessesByName("chromedriver");
            foreach (var chromeDriverProcess in chromeDriverProcesses)
            {
                chromeDriverProcess.Kill();
            }
            if (url.Equals(txtUrl.Text + "/Home.jsp"))
            {
                return true;
            }
            return false;
        }

        void work()
        {
            Process[] chromeDriverProcesses;
            running = true;
            var driverService = ChromeDriverService.CreateDefaultService();
            driverService.HideCommandPromptWindow = true;
            ChromeOptions options = new ChromeOptions();
            options.AddArgument("--disable-extensions");
            IWebDriver driver = new ChromeDriver(driverService, options);
            driver.Navigate().GoToUrl(txtUrl.Text+txtConst.Text);
            driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(2));
            string url = driver.Url.ToString();
            if (url.Equals(txtUrl.Text + "/Home.jsp"))
            {
                driver.Close();
                chromeDriverProcesses = Process.GetProcessesByName("chromedriver");
                foreach (var chromeDriverProcess in chromeDriverProcesses)
                {
                    chromeDriverProcess.Kill();
                }
                running = false;
                return;
            }
            try
            {
                driver.FindElement(By.Name("Username")).SendKeys(txtUser.Text);
                driver.FindElement(By.Name("Password")).SendKeys(txtPwd.Text);
            }
            catch(Exception ex)
            {
                chromeDriverProcesses = Process.GetProcessesByName("chromedriver");

                foreach (var chromeDriverProcess in chromeDriverProcesses)
                {
                    chromeDriverProcess.Kill();
                }
                running = false;
                driver.Close();
                return;
            }
            driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(3));
            IWebElement element = driver.FindElement(By.XPath("//input[@type='submit']"));
            if (element.Displayed)
            {
                forceClick(element);
            }
            driver.Close();
            running = false;
            chromeDriverProcesses = Process.GetProcessesByName("chromedriver");

            foreach (var chromeDriverProcess in chromeDriverProcesses)
            {
                chromeDriverProcess.Kill();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if ((txtPwd.TextLength == 0)||(txtUrl.TextLength == 0)||(txtUser.TextLength == 0))
            {
                MessageBox.Show("Please fill out all the fields!");
                return;
            }
            if (button1.Text.Equals("Enable"))
            {
                txtPwd.Enabled = false;
                txtUser.Enabled = false;
                txtUrl.Enabled = false;
                txtConst.Enabled = false;
                timer1.Enabled = true;
                button1.Text = "Disable";
            }
            else
            {
                txtPwd.Enabled = true;
                txtUser.Enabled = true;
                txtUrl.Enabled = true;
                txtConst.Enabled = true;
                timer1.Enabled = false;
                button1.Text = "Enable";
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (CheckForInternetConnection() == false)
            {
                /*if (check2() == false)
                {
                    
                }*/
                if (!running)
                {
                    Task.Run(() => work());
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                System.IO.File.WriteAllText("info.dat", string.Empty);
            }
            catch(Exception ex)
            {

            }
            var fs = File.Open("info.dat", FileMode.OpenOrCreate, FileAccess.ReadWrite);
            var sw = new StreamWriter(fs);
            sw.WriteLine(txtUrl.Text);
            sw.WriteLine(txtUser.Text);
            sw.WriteLine(txtPwd.Text);
            sw.Close();
            fs.Close();
            MessageBox.Show("Saved!");
        }

        private void frmMain_Resize(object sender, EventArgs e)
        {
            if (FormWindowState.Minimized == this.WindowState)
            {
                notifyIcon1.Visible = true;
                notifyIcon1.ShowBalloonTip(500);
                this.Hide();
            }

            else if (FormWindowState.Normal == this.WindowState)
            {
                notifyIcon1.Visible = false;
            }
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Show();
            this.WindowState = FormWindowState.Normal;
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void notifyIcon1_Click(object sender, EventArgs e)
        {
            this.Show();
            this.WindowState = FormWindowState.Normal;
        }

    }
}
